import 'package:flutter/material.dart';
import './result.dart';
import './quiz.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  final questions = const [
    {
      'questionText': 'What\'s your favourite colour?',
      'answers': [
        {'text': 'Black', 'score': 1},
        {'text': 'Red', 'score': 2},
        {'text': 'Yellow', 'score': 3},
        {'text': 'Blue', 'score': 4}
      ],
    },
    {
      'questionText': 'What\'s your favourite animal?',
      'answers': [
        {'text': 'Lion', 'score': 1},
        {'text': 'Dog', 'score': 2},
        {'text': 'Tiger', 'score': 3},
        {'text': 'Bird', 'score': 4}
      ],
    },
    {
      'questionText': 'Which\' your favorite number?',
      'answers': [
        {'text': 'one', 'score': 1},
        {'text': 'two', 'score': 2},
        {'text': 'three', 'score': 3},
        {'text': 'four', 'score': 4}
      ],
    }
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _answerQuestion(int score) {
    _totalScore += score;
    
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    //print(_questionIndex);
    print(_totalScore);
  }

  void _resetQuiz(){
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('My First App'),
          ),
          body: _questionIndex < questions.length
              ? Quiz(
                  questionIndex: _questionIndex,
                  questions: questions,
                  answerQuestion: _answerQuestion,
                )
              : Result(_totalScore, _resetQuiz)),
    );
  }
}
