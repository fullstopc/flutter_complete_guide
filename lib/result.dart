import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    String resultText;
    if (resultScore <= 3) {
      resultText = 'less than 3';
    } else if (resultScore <= 5) {
      resultText = 'less than 5';
    } else {
      resultText = 'highscore';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: <Widget>[
        Text(
          resultPhrase,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
        ),
        FlatButton(
          child: Text(
            'Restart Quiz!',
            style: TextStyle(color: Colors.blue),
          ),
          onPressed: resetHandler,
        )
      ],
    ));
  }
}
